package com.cursoandroid.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class Flappybird extends ApplicationAdapter {

    private SpriteBatch batch;
    private Texture[] passaros;
    private Texture fundo;
    private Texture canoBaixo;
    private Texture canoTopo;
    private Texture gameOver;

    private int larguraDispositivo;
    private int alturaDispositivo;
    private int estadoJogo = 0; //0- jogo nao iniciado 1- jogo iniciado
    private int pontuacao = 0;

    private Random numeroAleatorio;
    private BitmapFont fonte;
    private BitmapFont mensagem;
    private Circle passaroCirculo;
    private Rectangle retangulocanoTopo;
    private Rectangle retangulocanoBaixo;
    //private ShapeRenderer shape;

    private boolean marcouPonto;

    private float variacao = 0;
    private float velocidadeQueda = 0;
    private float posicaoMovimentoCanoHorizontal;
    private float posicaoMovimentoCanoVertical;
    private float espacoEntreCanos;
    private int posicaoInicialVertical = 0;
    private float deltaTime;
    private float alturaEntreCanosRandomica;

	@Override
	public void create () {
        batch = new SpriteBatch();
        numeroAleatorio = new Random();
        //passaro  = new Texture("passaro1.png");
        passaros = new Texture[3];
        passaros[0] = new Texture("passaro1.png");
        passaros[1] = new Texture("passaro2.png");
        passaros[2] = new Texture("passaro3.png");
        fundo = new Texture("fundo.png");
        retangulocanoBaixo = new Rectangle();
        retangulocanoTopo = new Rectangle();
        passaroCirculo = new Circle();
        //shape = new ShapeRenderer();
        canoBaixo = new Texture("cano_baixo.png");
        canoTopo = new Texture("cano_topo.png");
        fonte = new BitmapFont();
        mensagem = new BitmapFont();
        mensagem.setColor(Color.WHITE);
        mensagem.getData().setScale(3);
        fonte.setColor(Color.WHITE);
        fonte.getData().setScale(6);
        gameOver = new Texture("game_over.png");

        larguraDispositivo = Gdx.graphics.getWidth();
        alturaDispositivo = Gdx.graphics.getHeight();
        posicaoInicialVertical = alturaDispositivo/2;
        posicaoMovimentoCanoHorizontal = larguraDispositivo - 100;
        espacoEntreCanos = 300;
	}

	@Override
	public void render () {

        deltaTime = Gdx.graphics.getDeltaTime();
        variacao += deltaTime * 10;
        if (variacao > 2) {
            variacao = 0;
        }

        if(estadoJogo == 0){
            if(Gdx.input.justTouched()){
                estadoJogo = 1;
            }

        }else {
            velocidadeQueda++;
            if (posicaoInicialVertical > 0 || velocidadeQueda < 0) {
                posicaoInicialVertical -= velocidadeQueda;
            }

            if(estadoJogo == 1){



                posicaoMovimentoCanoHorizontal -= deltaTime * 200;


                if (Gdx.input.justTouched()) {
                    velocidadeQueda = -15;
                }


                //verifica se o cano saiu inteiramente da tela
                if (posicaoMovimentoCanoHorizontal < -canoBaixo.getWidth()) {
                    posicaoMovimentoCanoHorizontal = larguraDispositivo - 100;
                    alturaEntreCanosRandomica = numeroAleatorio.nextInt(400) - 200;
                    marcouPonto = false;
                }

                if(posicaoMovimentoCanoHorizontal < 120){
                    if( !marcouPonto ){
                        pontuacao++;
                        marcouPonto = true;
                    }
                }
            }
            else{

                if(Gdx.input.justTouched()){
                    estadoJogo = 0;
                    pontuacao = 0;
                    velocidadeQueda = 0;
                    posicaoInicialVertical = alturaDispositivo / 2;
                    posicaoMovimentoCanoHorizontal = larguraDispositivo;
                }

            }
        }

        batch.begin();
        batch.draw(fundo, 0, 0, larguraDispositivo, alturaDispositivo);
        batch.draw(canoTopo, posicaoMovimentoCanoHorizontal, alturaDispositivo / 2 + espacoEntreCanos / 2 + alturaEntreCanosRandomica);
        batch.draw(canoBaixo, posicaoMovimentoCanoHorizontal, alturaDispositivo / 2 - canoBaixo.getHeight() - espacoEntreCanos / 2 + alturaEntreCanosRandomica);
        batch.draw(passaros[(int) variacao], 120, posicaoInicialVertical);
        fonte.draw(batch, String.valueOf(pontuacao),larguraDispositivo / 2, alturaDispositivo - 50);

        if(estadoJogo == 2){
            batch.draw(gameOver, larguraDispositivo / 2 - gameOver.getWidth() / 2, alturaDispositivo / 2);
            mensagem.draw(batch,"Toque para Reiniciar!", larguraDispositivo / 2 - 200, alturaDispositivo / 2 - gameOver.getHeight() / 2);
        }


        batch.end();


        passaroCirculo.set(120 + passaros[0].getWidth() / 2, posicaoInicialVertical + passaros[0].getHeight() / 2, passaros[0].getWidth() / 2);
        retangulocanoBaixo = new Rectangle( posicaoMovimentoCanoHorizontal,
                alturaDispositivo / 2 - canoBaixo.getHeight() - espacoEntreCanos / 2 + alturaEntreCanosRandomica,
                canoBaixo.getWidth(),
                canoBaixo.getHeight());

        retangulocanoTopo = new Rectangle(posicaoMovimentoCanoHorizontal,
                alturaDispositivo / 2 + espacoEntreCanos / 2 + alturaEntreCanosRandomica,
                canoTopo.getWidth(),
                canoTopo.getHeight());
        /*shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.circle( passaroCirculo.x, passaroCirculo.y, passaroCirculo.radius);
        shape.setColor(Color.RED);
        shape.rect(retangulocanoBaixo.x,retangulocanoBaixo.y,retangulocanoBaixo.width,retangulocanoBaixo.height);
        shape.rect(retangulocanoTopo.x,retangulocanoTopo.y,retangulocanoTopo.width,retangulocanoTopo.height);

        shape.end();*/


        if(Intersector.overlaps(passaroCirculo, retangulocanoBaixo) || Intersector.overlaps(passaroCirculo, retangulocanoTopo)){
            estadoJogo = 2;
        }

	}

}
